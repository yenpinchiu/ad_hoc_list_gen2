package com.appier.ad_hoc_list_gen2

import org.apache.spark.storage.StorageLevel.MEMORY_AND_DISK_SER
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.{udf, count, explode}

import scala.util.{Try, Success, Failure}
import org.json4s.{JValue, JString, JNothing, JArray}
import org.json4s.jackson.JsonMethods.parse

import org.rogach.scallop._

import com.appier.cd_object.utilities.{S3Util, DateUtil, IDUtil}

object TimeDistribution {
  val task_name = this.getClass.getSimpleName.init

  def main(args: Array[String]) = {
    object conf extends ScallopConf(args) {
      val infile = opt[String](required = true)
      val filetype = opt[String](default = Option("json"))
      val country = opt[String](required = true)
      val refdate = opt[String](required = true)
      val logfile = opt[String](required = true)
      val npart = opt[Int](default = Option(320))
      val title = opt[String]()
      verify()
    }

    val infile = conf.infile()
    val filetype = conf.filetype()
    val country = conf.country()
    val refdate = conf.refdate()
    val logfile = conf.logfile()
    val npart = conf.npart()
    val opt_title = conf.title.get

    val title = (Option(task_name) ++ opt_title).mkString(" ")

    val sparkConf = JobUtil.defaultConf(
      title,
      settings = Some(Seq(
        ("parquet.enable.summary-metadata", "false"),
        ("spark.sql.shuffle.partitions", "%d".format(npart)),
        ("spark.speculation", "true"),
        ("spark.speculation.multiplier", "3")
      ))
    )

    val spark = SparkSession
      .builder()
      .config(sparkConf)
      .getOrCreate()
    import spark.implicits._

    val udfIDsFromJson = udf((s: String) =>
      Try {
        (parse(s) \ "ids" \ "idfa") match {
          case JArray(l) => Some(l.map { case JString(x) => x }.toSeq)
          case _         => None
        }
      } match {
        case Success(result) => result
        case Failure(ex)     => None
      })
    val udfIDFromCSV = udf((s: String) =>
      Try {
        Some(s.split(',')(1))
      } match {
        case Success(result) => result
        case Failure(ex)     => None
      })

    val udfIDFromIdList = udf((s: String) =>
      Try {
        Some(s)
      } match {
        case Success(result) => result
        case Failure(ex)     => None
      })

    val udfIsIDFA = udf((s: String) => IDUtil.parseIDFormat(s) == IDUtil.TagIDFA)

    val s3 = S3Util()

    val inputs = spark.read
      .text(infile)

    val ids = filetype match {
      case "json" =>
        inputs
          .select(
            explode(udfIDsFromJson($"value")) as "id"
          )
          .filter("id IS NOT NULL")
          .filter(udfIsIDFA($"id"))
          .distinct
      case "id_list" =>
        inputs
          .select(
            udfIDFromIdList($"value") as "id"
          )
          .filter("id IS NOT NULL")
          .filter(udfIsIDFA($"id"))
          .distinct
      case _ =>
        inputs
          .select(
            udfIDFromCSV($"value") as "id"
          )
          .filter("id IS NOT NULL")
          .filter(udfIsIDFA($"id"))
          .distinct
    }

    ids.persist(MEMORY_AND_DISK_SER)

    val (basePath, activePaths) = getActivePaths(refdate, country)

    val nTotalIds = ids.count
    val tmp = spark.read
      .option("basePath", basePath)
      .parquet(activePaths: _*)
      .select(
        $"id",
        explode($"active_168hours")
      )
      .select(
        $"id",
        $"key" as "active_168hours"
      )
      .join(ids, Seq("id"), "left_semi")
      .distinct
      .persist(MEMORY_AND_DISK_SER)
    val nJoinID = tmp.select($"id").distinct.count
    val log = tmp
      .groupBy("active_168hours")
      .agg(
        count($"id") as "n_id"
      )
      .as[(Int, Long)]
      .collect
      .sortWith(_._1 < _._1)
      .map {
        case (hour, cnt) =>
          "%d\t%d".format(hour, cnt)
      }.mkString("nTotalIds: %d\nnJoinIds: %d\n".format(nTotalIds, nJoinID), "\n", "\n")

    s3.write(logfile, log)
  }

  def getActivePaths(
    refdate: String,
    country: String
  ): (String, List[String]) = {
    val basePath = "s3a://appier-cd-attribute/out/device_active_time/version=0.1"
    val s3 = S3Util()
    val paths = (0 to 10).toList.flatMap { w =>
      val date = DateUtil.dateAdd(refdate, w * (-7))
      val path = s"${basePath}/date=${date}/country=${country}"
      if (s3.exists(path + "/_SUCCESS")) Some(path) else None
    }
    (basePath, paths)
  }
}
