#!/mnt/miniconda/envs/next_page/bin/python
# -*- coding: utf-8 -*-

from __future__ import unicode_literals, absolute_import, division, print_function

import time
from datetime import datetime
from datetime import timedelta as t

from cron_utils.args import generate_args
from cron_utils.args import sub

from common import default_parser, local_class, submit_job

import val

infile_format = val.v_general_infile
logfile_format = val.v_general_infile + '_log'

def main(host, status, priority, retry, pool, group, stage, date, country, refdate, filetype, filename, parts_per_job):
    file_name = 'TimeDistribution'
    class_name = local_class(file_name)
    job_name = sub('[ad_hoc_list_gen] {file_name}')

    infile = sub(infile_format)
    logfile = sub(logfile_format)

    inputs = []
    outputs = []

    args = dict(
        date=date,
        country=country,
        refdate=refdate,
        filetype=filetype,
        filename=filename)

    title = sub('({date},{country},{refdate},{filetype},{filename})')

    cmd_args = generate_args(
        infile=infile,
        filetype=filetype,
        country=country,
        refdate=refdate,
        npart=parts_per_job,
        logfile=logfile)

    submit_job(
        class_name,
        job_name,
        title,
        status,
        priority,
        retry,
        inputs=inputs,
        outputs=outputs,
        host=host,
        cmd_args=cmd_args,
        args=args,
        pool=pool,
        group=group,
        stage=stage)

if __name__ == '__main__':
    parser = default_parser()
    parser.add_argument('-d', '--date', required=True)
    parser.add_argument('-c', '--country', required=True)
    parser.add_argument('-R', '--refdate', required=True)
    parser.add_argument('-t', '--filetype', required=True)
    parser.add_argument('-f', '--filename', required=True)
    parser.add_argument('-n', '--parts-per-job', type=int, default=320)
    args = parser.parse_args()

    main(
        host=args.host,
        status=args.status,
        priority=args.priority,
        retry=args.retry,
        pool=args.pool,
        group='ad_hoc_list_gen2',
        stage=args.stage, 
        date=args.date,
        country=args.country,
        refdate=args.refdate,
        filetype=args.filetype,
        filename=args.filename,
        parts_per_job=args.parts_per_job,
    )
