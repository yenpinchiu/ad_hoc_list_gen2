Example Spark Project
======================

## bootstrap (only for first time)
```bash
$ git clone git@bitbucket.org:plaxieappier/ad_hoc_list_gen2.git
$ mv ad_hoc_list_gen2 your_project_name
$ cd your_project_name
$ ./bootstrap
```

This would modify the project to be under the `your_project_name` namespace.
In addition, it modifies git config to point to `https://bitbucket.org/plaxieappier/your_project_name`.

## Set Up Python Virtual Environment
Modify `requirements.txt` and execute
```bash
$ make setup
```

This would install a virtual environment under `/mnt/miniconda/envs/your_project_name` on local machine and spark masters.

## Additional files/folders for shipping
If you create additional files/folders needed to be shipped with your program, 
please edit `include-list.txt` to add them.

## Compile and Deploy
If you need to do access python virtual env in your spark program or run python on spark masters:

    make package # zip your python virtual env

Notice: you may need to do this too if you invoked `make clean`.

At spark master machines such as master-ai-general.spark.appier.info

    make build # compile
    make install # deploy to S3

The jars would be installed at `s3://appier-cd-release/release/ad_hoc_list_gen2`.

    make stage # deploy to staging S3 location

The jars would be installed at `s3://appier-cd-release/stage/ad_hoc_list_gen2`.

## Configure Scalafmt Code Formatting

Create a `.scalafmt.conf` at the root directory of the project. E.g.,

    style = defaultWithAlign
    align.openParenCallSite = false
    align.openParenDefnSite = false
    align.tokens = [{code = "->"}, {code = "<-"}, {code = "=>", owner = "Case"}]
    continuationIndent.callSite = 2
    continuationIndent.defnSite = 2
    danglingParentheses = true
    indentOperator = spray
    maxColumn = 120
    newlines.alwaysBeforeTopLevelStatements = true
    project.excludeFilters = [".*\\.sbt"]
    rewrite.rules = [RedundantParens, SortImports]
    spaces.inImportCurlyBraces = false
    unindentTopLevelOperators = true