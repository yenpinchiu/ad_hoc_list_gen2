#!/mnt/miniconda/envs/ad_hoc_list_gen2/bin/python
# -*- coding: utf-8 -*-

from __future__ import unicode_literals, absolute_import, division, print_function

v_hello_out = 's3a://{dev_bucket}/out/{date}/{country}/ad_hoc_list_gen2/hello.txt'

v_general_infile = 's3a://appier-query-result-0/out/{date}/{filename}'
