#!/mnt/miniconda/envs/ad_hoc_list_gen2/bin/python
# -*- coding: utf-8 -*-

from __future__ import unicode_literals, absolute_import, division, print_function

from cron_utils.scheduler import submit_job as remote_submit
from cron_utils.scheduler import default_parser as remote_parser
from cron_utils.scheduler import spark_classpath as remote_classpath


def spark_classpath(stage=False):
    return remote_classpath(
        project='ad_hoc_list_gen2',
        stage=stage,
        package='ad_hoc_list_gen2',
        scala_version='2.11',
        version='0.1-spark-2.2.0-SNAPSHOT',
        suppress_search=True)


def local_class(cls):
    return 'com.appier.ad_hoc_list_gen2.' + cls


def submit_job(class_name,
               job_name,
               title,
               status,
               priority,
               retry,
               cmd_args='',
               args=None,
               inputs=None,
               outputs=None,
               host=None,
               pool=None,
               group=None,
               stage=False):
    classpath = spark_classpath(stage)
    s3_path = 's3://appier-cd-release/{env}/ad_hoc_list_gen2'.format(env='stage' if stage else 'release')

    remote_submit(
        classpath,
        class_name,
        job_name,
        title,
        status,
        priority,
        retry,
        s3_path,
        cmd_args,
        args,
        inputs,
        outputs,
        host,
        pool,
        group,
        timeout=8 * 60 * 60)


def default_parser(host='master-cd-general.spark.appier.info', group='ad_hoc_list_gen2'):
    return remote_parser(
        host=host, status='pending', priority=51, retry=0, pool=256, group=group)
